
//
//  ViewController.swift
//  NewExpenseTracker
//
//  Created by Randy Jorgensen on 2/13/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

import UIKit
import MessageUI


class ViewController: UIViewController, UITableViewDataSource {
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
     
    }
    
    
    @IBOutlet weak var tableView: UITableView!
    
    var strDate = ""
    
   
    
    @IBOutlet weak var addButton: UIBarButtonItem!
    
    var newData = [[String: String]]()

    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int
    {
        return self.newData.count;
    }
    func tableView(tableView: UITableView,
        cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell
    {
        let cell = UITableViewCell()
        
        var a: String
        var myarray: [String] = []
        let index = indexPath.row
        for label in newData {
            let lAmount = label["amount"]! as String
            let lLocation = label["location"]! as String
            let lDate = label["date"]! as String
           
            
            a = ("\(lLocation) \(lAmount)  \(lDate)")
            
            myarray.append(a)
            
        }
        
        cell.textLabel!.text  = myarray[index]
        return cell
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        
       
        newData = Singleton.sharedInstance().myData
        
        if newData.count > 0 {
            self.tableView.reloadData()
             dump(newData)
        } else {
            return
        }
        
        dump(tableView)
        
    }
    
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}

