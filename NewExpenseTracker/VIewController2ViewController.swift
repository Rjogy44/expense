//
//  VIewController2ViewController.swift
//  NewExpenseTracker
//
//  Created by Randy Jorgensen on 2/21/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

import UIKit
import MessageUI

class VIewController2ViewController: UIViewController, MFMailComposeViewControllerDelegate  {

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
    }
    @IBOutlet var amount: UITextField! = UITextField()
    @IBOutlet var location: UITextField! = UITextField()
    @IBOutlet var picker: UIDatePicker! = UIDatePicker()
    
    var myData1 = [String: String]()
    var strDate: String = ""
    
    
    @IBAction func saveButton(sender: UIButton) {
        
        
        myData1 = ["amount" : self.amount.text!, "location" : self.location.text!, "date" :strDate]
        Singleton.sharedInstance().appendDictionary(myData1)
    
    }
    @IBAction func datePickerAction(sender: AnyObject) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "MMM-yy"
        strDate = dateFormatter.stringFromDate(picker.date)
        
    }
    
    @IBAction func MailButtonTapped(sender: UIButton) {
        let mailComposeViewController = configuredMailComposeViewController()
        if MFMailComposeViewController.canSendMail() {
            self.presentViewController(mailComposeViewController, animated: true, completion: nil)
        } else {
            self.showSendMailErrorAlert()
        }
    }
    
    
    
    func configuredMailComposeViewController()-> MFMailComposeViewController {
        let mailComposerVC = MFMailComposeViewController()
        mailComposerVC.mailComposeDelegate = self // Extremely important to set the --mailComposeDelegate-- property, NOT the --delegate-- property
        
        mailComposerVC.setToRecipients(["randyjorgensen44@gmail.com"])
        mailComposerVC.setSubject("Sending you an in-app e-mail...")
        mailComposerVC.setMessageBody("Sending e-mail in-app is not so bad!", isHTML: false)
        
        return mailComposerVC
    }
    
    func showSendMailErrorAlert() {
        let alertController = UIAlertController(title: "Could Not Send Mail", message: "Mail Did Not Send, Please try again!", preferredStyle: .Alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .Cancel) { (action) in
            
        }
        alertController.addAction(cancelAction)
        
        let OKAction = UIAlertAction(title: "OK", style: .Default) { (action) in
            
        }
        alertController.addAction(OKAction)
        
        self.presentViewController(alertController, animated: true) {
            
        }
    }
    func mailComposeController(controller: MFMailComposeViewController) {
        controller.dismissViewControllerAnimated(true, completion: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "ShowMainPage" {
            
            
            
        }
    }

}
