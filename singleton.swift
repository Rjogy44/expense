//
//  singleton.swift
//  NewExpenseTracker
//
//  Created by Randy Jorgensen on 2/21/16.
//  Copyright © 2016 Randy Jorgensen. All rights reserved.
//

import Foundation

class Singleton {
    static var instance: Singleton!
    var myData = [[String: String]]()
    
    // SHARED INSTANCE
    class func sharedInstance() -> Singleton {
        self.instance = (self.instance ?? Singleton())
        return self.instance
    }
    
    // METHODS
    init() {
    }
    func appendDictionary(dic: [String: String]) {
        self.myData.append(dic)
    }
}